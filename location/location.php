<?php
/*
Plugin Name: Location
Description: Menu creator
Version: 1.0
Author: [bvblogic]
Author URI: http://bvblogic.com/
*/


/*
|--------------------------------------------------------------------------
| CONSTANTS
|--------------------------------------------------------------------------
*/

if ( ! defined( 'RC_TC_BASE_FILE' ) )
    define( 'RC_TC_BASE_FILE', __FILE__ );
if ( ! defined( 'RC_TC_BASE_DIR' ) )
    define( 'RC_TC_BASE_DIR', dirname( RC_TC_BASE_FILE ) );
if ( ! defined( 'RC_TC_PLUGIN_URL' ) )
    define( 'RC_TC_PLUGIN_URL', plugin_dir_url( __FILE__ ) );


// Stop direct call
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }

if (!class_exists('my_menu')) {
    class my_menu {
        // data
        public $data = array();
        // object constructor
        // initialize of properties
        function my_menu()
        {
            global $wpdb;

            // constant of initialize plugin
            DEFINE('my_menu', true);

            // plusgin name
            $this->plugin_name = plugin_basename(__FILE__);

            // URL of plugin
            $this->plugin_url = trailingslashit(WP_PLUGIN_URL.'/'.dirname(plugin_basename(__FILE__)));

            // db table
            $this->tbl_city = $wpdb->prefix.'city';

            // activation
            register_activation_hook( $this->plugin_name, array(&$this, 'activate') );

            // deactivation
            register_deactivation_hook( $this->plugin_name, array(&$this, 'deactivate') );

            //  uninstall
            register_uninstall_hook( $this->plugin_name, array(&$this, 'uninstall') );

        }

        /**
         * plugin activation
         */
        function activate()
        {
            global $wpdb;
            $wpdb->show_errors();
            require_once(ABSPATH . 'wp-admin/upgrade-functions.php');

        }

        /**
         * plugin deactivation
         */
        function deactivate()
        {
            return true;
        }

        /**
         * plugin unisnstall
         */
        function uninstall()
        {
        }

    }


}

global $unit;
$unit = new my_menu();

add_action( 'init', 'build_taxonomies', 0 );

function build_taxonomies() {
    register_taxonomy('item_location', 'page', array(
        // Hierarchical taxonomy (like categories)
        'hierarchical' => true,
        // This array of options controls the labels displayed in the WordPress Admin UI
        'labels' => array(
            'name' => _x( 'Location', 'taxonomy general name' ),
            'singular_name' => _x( 'Location', 'Llocation singular name' ),
            'search_items' =>  __( 'Search Location' ),
            'all_items' => __( 'All Location' ),
            'parent_item' => __( 'Parent Location' ),
            'parent_item_colon' => __( 'Parent Location' ),
            'edit_item' => __( 'Edit Location' ),
            'update_item' => __( 'Update Location' ),
            'add_new_item' => __( 'Add New Location' ),
            'new_item_name' => __( 'New Location Name' ),
            'menu_name' => __( 'Location' ),
        ),
        // Control the slugs used for this taxonomy
        'rewrite' => array(
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'show_ui' => true,
            'slug' => 'item_location',
            'with_front' => true,
            'capability_type' => 'page',
            'hierarchical' => true
        ),
    ));

}


add_filter( 'template_include', 'include_template_function', 1 );

function include_template_function( $template_path ) {
    global $post;
    $data_page_terms = get_the_terms($post->id, 'item_location');
    if ( $data_page_terms ) {
        $template_path = plugin_dir_path( __FILE__ ) . '/single.php';
    }
    return $template_path;
}
