<?php
/*Template Name: Single Template
*/
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

get_header(); ?>

<div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">

        <?php
        $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
        ?>

        <header>
            <h1 class="page-title"><?php  echo $term->name ; ?></h1>
        </header>

        <article>
            <?php
            wp_list_categories('taxonomy=item_location&depth=1&title_li=&child_of=' . $term->term_id);
            ?>
        </article>

    </div>
</div>

<?php get_footer(); ?>
